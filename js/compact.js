/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      $("li.php-is-active .is-dropdown-submenu").addClass("js-dropdown-active");
      $("li.php-is-active").addClass("is-active");
      $( "#block-mainnavigation" ).hover(
        function() {
        }, function() {
          $("li.php-is-active .is-dropdown-submenu").addClass("js-dropdown-active");
          $("li.php-is-active").addClass("is-active");
          //$("li.php-is-active").trigger("hover");
        }
      );
    }
  };

})(jQuery, Drupal);
